package org.jz.model

import org.specs2.mutable.Specification
import scala.math._

/**
 * author: jz
 */
class BitBoardSpec extends Specification {


  "BitBoard" can {

    "be created with size up to 8x8" in {
      BitBoard(8, 8) must not(throwA[Throwable])
      BitBoard(8, 9) must throwA[AssertionError]
      BitBoard(9, 8) must throwA[AssertionError]
      BitBoard(8, -1) must throwA[AssertionError]
      BitBoard(-1, 8) must throwA[AssertionError]
    }

    "generate array indexes" in {
      val bb = BitBoard(4, 3)
      bb.idx(0, 0) === 0
      bb.idx(0, 1) === 4
      bb.idx(0, 2) === 8
      bb.idx(2, 0) === 2
      bb.idx(3, 2) === 11
    }

    "switch bits to desired value" in {
      val bb = BitBoard(4, 3)
      bb.bit(0, 0, true) === 1
      bb.bit(0, 1, true) === pow(2, 4).toLong
      bb.bit(0, 2, true) === pow(2, 8).toLong
      bb.bit(2, 0, true) === pow(2, 2).toLong
      bb.bit(3, 2, true) === pow(2, 11).toLong
    }

    "have bits set to desired value [1]" in {
      val actual = BitBoard(4, 3).set(3, 2, true).set(1, 2, true).toString
      val expected =
        """
          |____
          |____
          |_*_*
        """.stripMargin.trim
      actual === expected
    }

    "have bits set to desired value [2]" in {
      val actual = BitBoard(4, 3).set((3, 2) :: (1, 2) :: Nil, true).toString
      val expected =
        """
          |____
          |____
          |_*_*
        """.stripMargin.trim
      actual === expected
    }

    "get value of a bit" in {
      val bb = BitBoard(4, 3).set((3, 2) :: (1, 2) :: Nil, true)
      bb.get(0, 0) === bb.off
      bb.get(3, 2) === bb.on

    }

    "be negated" in {
      val bb = !(BitBoard(4, 3).set(3, 2, true).set(1, 2, true))
      val actual = bb.toString
      val expected =
        """
          |****
          |****
          |*_*_
        """.stripMargin.trim
      actual === expected
    }

    "be added to another bitboard of the same size" in {
      val bb1 = BitBoard(4, 3).set(3, 2, true).set(1, 2, true)
      val bb2 = BitBoard(4, 3).set(3, 2, true).set(1, 1, true)
      val actual = (bb1 | bb2).toString
      val expected =
        """
          |____
          |_*__
          |_*_*
        """.stripMargin.trim
      actual === expected
    }

    "be multiplied with another bitboard" in {
      val bb1 = BitBoard(4, 3).set(3, 2, true).set(1, 2, true)
      val bb2 = BitBoard(4, 3).set(3, 2, true).set(1, 1, true)
      val actual = (bb1 & bb2).toString
      val expected =
        """
          |____
          |____
          |___*
        """.stripMargin.trim
      actual === expected
    }

    "be xored with another bitboard of the same size" in {
      val bb1 = BitBoard(4, 3).set(3, 2, true).set(1, 2, true)
      val bb2 = BitBoard(4, 3).set(3, 2, true).set(1, 1, true)
      val actual = (bb1 ^ bb2).toString
      val expected =
        """
          |____
          |_*__
          |_*__
        """.stripMargin.trim
      actual === expected
    }

    "calculate number of 'on' bits" in {
      BitBoard(4, 3).set(3, 2, true).set(1, 2, true).weight === 2
    }

    "produce list of empty coordinates" in {
      val bb = !(BitBoard(4, 3).set(3, 2, true).set(1, 2, true))
      bb.emptyCoordinates === Stream(1 -> 2, 3 -> 2)
    }

    "calculate number of non-empty fields" in {
      val bb = !(BitBoard(4, 3).set(3, 2, true).set(1, 2, true))
      bb.weight === 10
    }

  }

}
