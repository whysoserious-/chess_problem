package org.jz.model

import org.specs2.mutable.Specification


/**
 * author: jz
 */
class BoardSpec extends Specification {

  "Board" can {

    "create empty board" in {
      val actual = Board.empty(3, 2).toString
      val expected =
        """
          |___
          |___""".stripMargin.trim
      actual === expected
    }

    "generate a stream of empty coordinates [1]" in {
      Board.empty(2,1).emptyCoordinates must containTheSameElementsAs(
        Seq(0 -> 0, 1 -> 0))
    }

    "generate a stream of empty coordinates [2]" in {
      Board.empty(4,4).emptyCoordinates must haveSize(16)
    }

    "not put a piece in invalid coordinates" in {
      Board empty(4,4) putPiece(King, 4, 4) should throwA[IndexOutOfBoundsException]
    }

    "not put 2 pieces in the same square" in {
      Board empty(4,4) putPiece(King, 3, 3) must beSome {
        board: Board => board putPiece(King, 3, 3) must beNone
      }
    }
    "not put a piece on a threatened square" in {
      Board empty(4,4) putPiece(King, 3, 3) must beSome {
        board: Board => board putPiece(King, 2, 3) must beNone
      }
    }

    "put 2 pieces in safe squares [1]" in {
      Board empty(4,4) putPiece(King, 3, 3) must beSome {
        board: Board => board putPiece(King, 0, 0) must beSome {
          actual: Board => {
            val expected =
              """
                |K*__
                |**__
                |__**
                |__*K""".stripMargin.trim
            actual.toString === expected
          }
        }
      }
    }

    "put 2 pieces in safe squares [2]" in {
      Board empty(2,2) putPiece(Rook, 0, 0) must beSome {
        board: Board => board putPiece(Rook, 1, 1) must beSome {
          actual: Board => {
            val expected =
              """
                |R*
                |*R""".stripMargin.trim
            actual.toString === expected
          }
        }
      }
    }

    "calculate number of non-empty fields" in {
      Board empty(2,2) putPiece(Rook, 0, 0) must beSome {
        board: Board => {
          board.emptyCount === 1
          board putPiece(Rook, 1, 1) must beSome {
            actual: Board => actual.emptyCount === 0
          }
        }
      }
    }

    "put a King" in {

      val actual = Board empty(4,4) putPiece(King, 1, 2)

      val expected =
        """
          |____
          |***_
          |*K*_
          |***_""".stripMargin.trim

      actual must beSome { board: Board => board.toString === expected }

    }

    "put a Rook" in {

      val actual = Board empty(3,3) putPiece(Rook, 1, 1)

      val expected =
        """
          |_*_
          |*R*
          |_*_""".stripMargin.trim

      actual must beSome { board: Board => board.toString === expected }

    }

    "put a Bishop" in {

      val actual = Board empty(4,4) putPiece(Bishop, 1, 2)

      val expected =
        """
          |___*
          |*_*_
          |_B__
          |*_*_""".stripMargin.trim

      actual must beSome { board: Board => board.toString === expected }

    }

    "put a Queen" in {

      val actual = Board empty(5,5) putPiece(Queen, 1, 2)

      val expected =
        """
          |_*_*_
          |***__
          |*Q***
          |***__
          |_*_*_""".stripMargin.trim

      actual must beSome { board: Board => board.toString === expected }

    }

    "put a Knight" in {

      val actual = Board empty(5,5) putPiece(Knight, 2, 2)

      val expected =
        """
          |_*_*_
          |*___*
          |__N__
          |*___*
          |_*_*_""".stripMargin.trim

      actual must beSome { board: Board => board.toString === expected }

    }

  }

}
