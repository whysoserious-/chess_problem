package org.jz

import org.specs2.mutable.Specification
import org.jz.model._
import scala.collection.immutable._

/**
 * author: jz
 */
class SolverSpec extends Specification {

  "Solver" should {

    import Solver._

    "solve [2, 2] R" in {
      solve(2, 2, Seq(Rook)) must haveSize(4)
    }

    "solve [2, 2] RR" in {
      solve(2, 2, Seq(Rook, Rook)) must haveSize(2)
    }

    "solve [2, 2] RRR" in {
      solve(2, 2, Seq(Rook, Rook, Rook)) must beEmpty
    }


    "solve [3, 3] KKR" in {
      solve(3, 3, Seq(King, King, Rook)) must haveSize(4)
    }

    "solve [4, 4] RRNNNN" in {
      solve(4, 4, Seq(Rook, Knight, Knight, Knight, Knight, Rook)) must haveSize(8)
    }

    "solve [5, 5] 5 queens" in {
      solve(5, 5, Seq.fill(5)(Queen)) must haveSize(10)
    }

    "solve [6, 6] 6 queens" in {
      solve(6, 6, Seq.fill(6)(Queen)) must haveSize(4)
    }

    "generate next states to visit" in {
      val board = Board.empty(3, 3).putPiece(King, 0, 0).get.putPiece(King, 2, 0).get
      val expected = Stream(State(board.putPiece(Rook, 1, 2).get, List()))
      val actual = Solver.statesToVisit(State(board, List(Rook)))
      actual === expected

    }

      //passes but it takes about 30 seconds to find all solutions
//    "solve [7, 7] 7 queens" in {
//      solve(7, 7, Seq.fill(7)(Queen)) must haveSize(40)
//    }
//
//    "solve [7, 7] KKQQBBN" in {
//      solve(7, 7, Seq(King, King, Queen, Queen, Bishop, Bishop, Knight)) must haveSize(-1)
//    }

  }

}
