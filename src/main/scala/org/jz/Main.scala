package org.jz

import org.jz.model._
import scala.collection.immutable._

/**
 * author: jz
 */
object Main extends App {

  if (args.size != 3) {
    println("Example usage: sbt run '4 4 KQBN'")
  } else {
    val results = Solver.solve(args(0).toInt, args(1).toInt, pieces(args(2)))
    println("First 10 solutions: ")
    results.take(10) foreach { result =>
      println("-----------------------");
      println(result)
    }
    println(s"FOUND ${results.size} SOLUTIONS")
  }

  def pieces(str: String): Iterable[Piece] = str map { Piece.fromChar(_) }

}
