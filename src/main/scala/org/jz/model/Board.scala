package org.jz.model

import scala.collection.immutable._

/**
 * author: jz
 */
object Board {

def pieceBoards(sx: Int, sy: Int) =  Map[Square, BitBoard](
  King -> BitBoard(sx, sy, King.print),
  Rook -> BitBoard(sx, sy, Rook.print),
  Bishop -> BitBoard(sx, sy, Bishop.print),
  Queen -> BitBoard(sx, sy, Queen.print),
  Knight -> BitBoard(sx, sy, Knight.print))

  def empty(sx: Int, sy: Int) = Board(
    sx,
    sy,
    pieceBoards(sx, sy),
    BitBoard(sx, sy),
    BitBoard(sx, sy, Threatened.print))

}

case class Board(sx: Int, sy: Int, boards: Map[Square, BitBoard], occupied: BitBoard, threatened: BitBoard) {

  val usedSquares = occupied | threatened

  def emptyCoordinates: Stream[(Int, Int)] = usedSquares.emptyCoordinates

  def emptyCount = sx * sy - usedSquares.weight

  def putPiece(piece: Piece, x: Int, y: Int): Option[Board] = occupied.isOn(x, y) match {
    case false => {
      val newThreats = piece.threatenedBitBoard(x, y, sx, sy)
      (occupied & newThreats).isEmpty match {
        case true => _put(piece, x, y, newThreats)
        case false => None
      }
    }
    case true => None
  }

  private def _put(piece: Piece, x: Int, y: Int, newThreats: BitBoard) : Option[Board] = {
    val updatedBoard = boards.get(piece) match {
      case Some(board) => {
        val updatedPieceBoard = board set(x, y, true)
        boards + (piece -> updatedPieceBoard)
      }
      case None => throw new IllegalArgumentException
    }
    val board = copy(
      boards = updatedBoard,
      threatened = threatened | newThreats,
      occupied = occupied.set(x, y, true)
    )
    Some(board)
  }

  override def toString = {
    val tmpBoards = threatened +: boards.values.toSeq
    val chars: Iterable[Char] = for {
      y <- 0 until sy
      x <- 0 until sx
    } yield {
      tmpBoards.find(board => board.get(x, y) == board.on) match {
        case Some(board) => board.get(x, y)
        case None => '_'
      }
    }
    chars.mkString.grouped(sx).mkString("\n")
  }

  private val _hashCode = boards.hashCode()

  override def hashCode(): Int = _hashCode

  override def equals(p1: scala.Any): Boolean = if (p1.isInstanceOf[Board]) {
    p1.asInstanceOf[Board].boards == boards
  } else {
    false
  }

}
