package org.jz.model

import scala.collection.immutable._

/**
 * author: jz
 */
sealed trait Square {

  def print: Char

}

case object Threatened extends Square {

  override def print = '*'

}

object Piece {

  def fromChar(char: Char): Piece = char match {
    case King.print => King
    case Rook.print => Rook
    case Bishop.print => Bishop
    case Queen.print => Queen
    case Knight.print => Knight
    case _ => throw new IllegalArgumentException
  }

}

sealed trait Piece extends Square {

  def threatenedBitBoard(x: Int, y: Int, sx: Int, sy: Int): BitBoard = {
    BitBoard(sx, sy).set(threatenedCoordinates(x, y, sx, sy), true)
  }

  def order: Int

  protected def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)]

}

case object King extends Piece {

  override val print = 'K'

  override val order: Int = 3

  override def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)] = {
    Stream(
      x - 1 -> (y - 1), x - 1 -> y, x - 1 -> (y + 1),
      x -> (y - 1), x -> (y + 1),
      x + 1 -> (y - 1), x + 1 -> y, x + 1 -> (y + 1)
    ) filter {
      case (x, y) => x >= 0 && y >= 0 && x < sx && y < sy
    }
  }

}

case object Rook extends Piece {

  override val print = 'R'

  override val order: Int = 1

  override def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)] = {
    val xView = (0 until x).toStream ++ ((x + 1) until sx) map (_ -> y)
    val yView = (0 until y).toStream ++ ((y + 1) until sy) map (x -> _)
    xView ++ yView
  }

}

case object Bishop extends Piece {

  override val print = 'B'

  override val order: Int = 2

  override def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)] = {
    val size = scala.math.min(sx, sy)
    val leftUp: Stream[(Int, Int)] = (1 until size).toStream map { idx =>
      (x - idx, y - idx)
    } takeWhile {
      case(x, y) => x >= 0 && y >= 0
    }
    val rightUp: Stream[(Int, Int)] = (1 until size).toStream map { idx =>
      (x - idx, y + idx)
    } takeWhile {
      case(x, y) => x >= 0 && y < sy
    }
    val leftDown: Stream[(Int, Int)] = (1 until size).toStream map { idx =>
      (x + idx, y - idx)
    } takeWhile {
      case(x, y) => x < sx && y >= 0
    }
    val rightDown: Stream[(Int, Int)] = (1 until size).toStream map { idx =>
      (x + idx, y + idx)
    } takeWhile {
      case(x, y) => x < sx && y  < sy
    }
    leftUp ++ rightUp ++ leftDown ++ rightDown
  }

}

case object Queen extends Piece {

  override val print = 'Q'

  override val order: Int = 0

  override def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)] = {
    Rook.threatenedCoordinates(x, y, sx, sy) ++ Bishop.threatenedCoordinates(x, y, sx, sy)
  }

}

case object Knight extends Piece {

  override val print = 'N'

  override val order: Int = 4

  override def threatenedCoordinates(x: Int, y: Int, sx: Int, sy: Int): Iterable[(Int, Int)] = {
    Stream(
      x - 2 -> (y - 1), x - 2 -> (y + 1),
      x + 2 -> (y - 1), x + 2 -> (y + 1),
      x + 1 -> (y + 2), x - 1 -> (y + 2),
      x + 1 -> (y - 2), x - 1 -> (y - 2)
    ) filter {
      case (x, y) => x >= 0 && y >= 0 && x < sx && y < sy
    }
  }

}




