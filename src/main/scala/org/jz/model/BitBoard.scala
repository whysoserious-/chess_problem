package org.jz.model

import scala.math._
import scala.collection.immutable.Iterable

/**
 * author: jz
 */
object BitBoard {

  implicit def booleanToLong(value: Boolean): Long = if (value) 1l else 0l

}

//least significant bit corresponds to index [0, 0]
case class BitBoard(sx: Int, sy: Int, on: Char = '*', off: Char = '_', board: Long = 0l) {

  import BitBoard._

  assert(sx > 0)
  assert(sy > 0)
  assert(sx <= 8)
  assert(sy <= 8)

  def set(x: Int, y: Int, value: Boolean): BitBoard = withCheck(x, y) { (x, y) =>
    copy(board = board | bit(x, y, value))
  }

  def set(points: Iterable[(Int, Int)], value: Boolean): BitBoard = {
    var mutableBoard = board
    points foreach { case (x, y) => mutableBoard = mutableBoard | bit(x, y, value) }
    copy(board = mutableBoard)
  }

  def isOn(x: Int, y: Int): Boolean = withCheck(x, y) { (x, y) =>
    (board & (1l << idx(x, y))) != 0
  }

  def get(x: Int, y: Int): Char = isOn(x, y) match {
    case true => on
    case false => off
  }

  def unary_! : BitBoard = copy(board = board ^ (pow(2, sx * sy).toLong - 1l))

  def |(bb: BitBoard): BitBoard = withCheck(bb) { bb => copy(board = board | bb.board) }

  def &(bb: BitBoard): BitBoard = withCheck(bb) { bb => copy(board = board & bb.board) }

  def ^(bb: BitBoard): BitBoard = withCheck(bb) { bb => copy(board = board ^ bb.board) }

  def isEmpty = board == 0

  def emptyCoordinates: Stream[(Int, Int)] = for {
    y <- (0 until sy).toStream
    x <- (0 until sx).toStream
    if !isOn(x, y)
  } yield {
    x -> y
  }

  //Hamming weight
  def weight: Long = {
    val m1  = 0x5555555555555555l
    val m2  = 0x3333333333333333l
    val h01 = 0x0101010101010101l
    val m4 = 0x0f0f0f0f0f0f0f0fl
    var x = board
    x -= (x >> 1) & m1;
    x = (x & m2) + ((x >> 2) & m2);
    x = (x + (x >> 4)) & m4;
    (x * h01) >> 56;
  }

  override def hashCode(): Int = board.hashCode()

  override def equals(p1: scala.Any): Boolean = if (p1.isInstanceOf[BitBoard]) {
    p1.asInstanceOf[BitBoard].board == board
  } else {
    false
  }

  override def toString = board.toBinaryString.reverse.map {
    case '1' => on
    case '0' => off
  }.padTo(sx * sy, off).grouped(sx).mkString("\n")

  private def withCheck(bb: BitBoard)(fun: BitBoard => BitBoard): BitBoard = if (bb.sx != sx || bb.sy != sy) {
    throw new IllegalArgumentException
  } else {
    fun(bb)
  }

  private def withCheck[T](x: Int, y: Int)(fun: (Int, Int) => T): T = if (x < sx && x >= 0 && y < sy && y >= 0) {
    fun(x, y)
  } else {
    throw new IndexOutOfBoundsException
  }

  @inline private[model] def idx(x: Int, y: Int): Int = y * sx + x

  @inline private[model] def bit(x: Int, y: Int, value: Boolean) = value << idx(x, y)

}




