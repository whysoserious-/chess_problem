package org.jz

import scala.collection.immutable._
import scala.annotation.tailrec
import org.jz.model.{Board, Piece}
import java.util.Date

/**
 * author: jz
 */
object Solver {

  private[jz] case class State(board: Board, pieces: Seq[Piece]) {
    def isSolution = pieces isEmpty
  }

  private[jz] def statesToVisit(state: State): Iterable[State] = state match {
    case State(_, Nil) => Nil
    case State(board, piece :: pieces) => {
      board.emptyCoordinates flatMap {
        case (x, y) => {
          board putPiece(piece, x, y)
        }
      } filter { board =>
        board.emptyCount >= pieces.size
      } map { board =>
        State(board, pieces)
      }
    }
  }


  def solve(x: Int, y: Int, pieces: Iterable[Piece]): Iterable[Board] = {

    val board = Board empty(x, y)
    val sortedPieces = (Seq() ++ pieces).sortBy(_.order)
    var toVisit = scala.collection.mutable.ArrayBuffer[State](State(board, sortedPieces))
    toVisit.sizeHint(100000)

    var cnt = 0

    @tailrec
    def loop(solutions: Set[Board], idx: Int): Iterable[Board] = {
      idx match {
        case idx if idx == toVisit.size => solutions
        case 50000 => {
          toVisit = toVisit.drop(50000)
          loop(solutions, 0)
        }
        case idx => {
          val state = toVisit(idx)
          toVisit ++= statesToVisit(state)
          cnt = cnt + 1
          if (cnt % 100000 == 0) {
            println(s"$cnt: ${new Date}")
            println(s"TO VISIT: ${toVisit.size}")
            println(s"SOLUTIONS: ${solutions.size}")
          }
          state match {
            case state if state.isSolution => loop(solutions + state.board, idx + 1)
            case _ => loop(solutions, idx + 1)
          }

        }
      }

    }

    loop(Set(), 0)

  }

}


