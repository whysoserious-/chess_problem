import sbt._
import Keys._

object ApplicationBuild extends Build {

  lazy val Dissector = Project(
    id = "chess_problem",
    base = file("."),
    settings = Defaults.defaultSettings ++ Seq(
      organization         := "org.jz",
      version              := "1.0",
      scalaVersion         := "2.10.3",
      crossPaths           := false,
      organizationHomepage := Some(url("http://about.me/jjjjjjjj")),
      scalacOptions        := Seq("-unchecked", "-deprecation", "-feature", "-language:postfixOps", "-language:implicitConversions"),
      scalacOptions in Test ++= Seq("-Yrangepos"),
      libraryDependencies  ++= Seq(
        "org.specs2" %% "specs2" % "2.3.3" % "test")))

}
